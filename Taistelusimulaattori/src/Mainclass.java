import java.util.Scanner;

public class Mainclass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner choice = new Scanner(System.in);
		Character hahmo = null;
		
		while (true) {

			System.out
					.println("*** TAISTELUSIMULAATTORI ***\n1) Luo hahmo\n2) Taistele hahmolla\n0) Lopeta\nValintasi: ");
			int valinta = choice.nextInt();
			
			if (valinta == 1) {
				hahmo = Character.valikkoHahmo(choice);
				hahmo.weapon = Character.valikkoAse(choice);
			} else if (valinta == 2) {
				Character.printFight(hahmo, hahmo.weapon);
			} else if (valinta == 0) {
				break;
			} else {
				System.out.println("Väärä valinta");
			}
		}
		choice.close();
	}

}
