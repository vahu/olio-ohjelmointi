import java.util.Scanner;

class Troll extends Character {
	public Troll() {
	}
}

class Knight extends Character {
	public Knight() {
	}

}

class Queen extends Character {
	public Queen() {
	}

}

class King extends Character {
	public King() {
	}

}

abstract class WeaponBehavior {
	public WeaponBehavior() {
	}

}

class Club extends WeaponBehavior {
	public Club() {
	}

}

class Sword extends WeaponBehavior {
	public Sword() {
	}

}

class Knife extends WeaponBehavior {
	public Knife() {

	}

}

class Axe extends WeaponBehavior {
	public Axe() {

	}
}

public abstract class Character {
	/*
	 * private Troll peikko; private Knight ritari; private Queen kuningatar;
	 * private King kuningas; public Axe kirves; public Knife puukko; public
	 * Sword miekka; public Club nuija;
	 */
	public WeaponBehavior weapon;

	public Character() {
		/*
		 * peikko = new Troll(); ritari = new Knight(); kuningatar = new
		 * Queen(); kuningas = new King(); kirves = new Axe(); puukko = new
		 * Knife(); miekka = new Sword(); nuija = new Club();
		 */
	}

	public static Character valikkoHahmo(Scanner choice) {
		//Scanner choice = new Scanner(System.in);
		System.out
				.println("Valitse hahmosi:\n1) Kuningas\n2) Ritari\n3) Kuningatar\n4)Peikko\nValintasi: ");
		Character hahmo = null;

		int valinta = choice.nextInt();
		switch (valinta) {
		case 1:
			hahmo = new King();
			break;
		case 2:
			hahmo = new Knight();
			break;
		case 3:
			hahmo = new Queen();
			break;
		case 4:
			hahmo = new Troll();
			break;
		default:
			System.out.print("Väärä valinta!");
		}
		//choice.close();
		return hahmo;
	}

	public static WeaponBehavior valikkoAse(Scanner choice2 ) {
		//= new Scanner(System.in);
		System.out
				.println("Valitse aseesi:\n1) Veitsi\n2) Kirves\n3) Miekka\n4)Nuija\nValintasi: ");
		WeaponBehavior weapon = null;
		int valinta2 = choice2.nextInt();
		switch (valinta2) {
		case 1:
			weapon = new Knife();
			break;
		case 2:
			weapon = new Axe();
			break;
		case 3:
			weapon = new Sword();
			break;
		case 4:
			weapon = new Club();
			break;
		default:
			System.out.print("Väärä valinta!");
		}
		//choice2.close();
		return weapon;
	}

	public static void printFight(Character c, WeaponBehavior w) {
		System.out.println(c.getClass().getSimpleName() + " tappelee aseella. "
				+ w.getClass().getSimpleName());
	}

}
