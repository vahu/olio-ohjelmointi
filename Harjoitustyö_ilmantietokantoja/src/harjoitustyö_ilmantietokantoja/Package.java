/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö_ilmantietokantoja;

import javafx.scene.control.Label;

/**
 *
 * @author varpu
 */
public abstract class Package {

    private  NewObject new_object;
    private  Label label;
    private  String name;
    private Pakettiautomaatti startAutomat;
    private Pakettiautomaatti endAutomat;
    double distance;
    int sendClass;
    private static int i = 0;

    public Package(NewObject some_object, Label some_label, Pakettiautomaatti pa1, Pakettiautomaatti pa2) {
        String index = Integer.toString(i++);
        name = "Paketti" + i;
        new_object = some_object;
        label = some_label;
        distance = calculateDistance(pa1,pa2);
        startAutomat = pa1;
        endAutomat = pa2;

    }
    
    public String toString(){
        return name + " joka sisältää esineen: " + new_object.getName();
    }
    
    
    //tällä metodilla lasketaan matkan pituus kahden automaatin välillä 
    public double calculateDistance(Pakettiautomaatti startA,Pakettiautomaatti endA){
        float lat1 = startA.getGeoPoint().getLattitude();
        float lon1 = startA.getGeoPoint().getLongitude();
        float lat2 = endA.getGeoPoint().getLattitude();
        float lon2 = endA.getGeoPoint().getLongitude();
        
        double angle = lon1-lon2;
        double distance = Math.sin(degTorad(lat1))*Math.sin(degTorad(lat2))+Math.cos(degTorad(lat1))*Math.cos(degTorad(lat2))*Math.cos(degTorad(angle));
    distance = Math.acos(distance);
        distance = radTodeg(distance);
        distance = distance * 60 * 1.1515 *1.609344;
        return distance;
    
    }
    
    //tällä metodilla muunnettaan asteina anettut kulmat radiaaneiksi
    private static double degTorad(double deg){
        return(deg*Math.PI/180.0);
    }
   
    //tällä metodilla muunnettaan radiaaneina anettut kulmat asteiksi
    private static double radTodeg (double rad){
        return(rad*180/Math.PI);
    }
    
    public String getName(){
        return name;
    }
    
    public int getSendClass(){
        return sendClass;
    }
    
    public Pakettiautomaatti getStartautomat(){
        return startAutomat;
    }
    public Pakettiautomaatti getEndautomat(){
        return endAutomat;
    }
    
    public double getDistance(){
        return distance;
    }
    
    public NewObject getObject(){
        return new_object;
    }

}

//kunkin luokan ehdot löytyvät näiden rakentajista

class FirstClass extends Package {
    
    public FirstClass(NewObject o, Label l,Pakettiautomaatti pa1, Pakettiautomaatti pa2) {
        super(o, l,pa1,pa2);
        l.setText("");
        sendClass=1;
        if (distance > 150.0){
            l.setText("Matka on liian pitkä tälle pakettiluokalle");
        }
        if (o.getIfFragile() == true) {
            l.setText("Särkyvää esinettä ei voi lähettää 1. luokalla. Valitse muu luokka.");
        }
        if (o.getWeight() > 10) {
            l.setText("Esine on liian painava 1. luokan paketille, valitse muu luokka.");
        }
        if (o.getHeight() > 70 || o.getWidth() > 70 || o.getDepth() > 80) {
            l.setText("Esine on liian iso 1. luokan paketille, sopiva luokka on 3. luokka.");
        }

    }
}

class SecondClass extends Package {

  
    public SecondClass(NewObject o, Label l,Pakettiautomaatti pa1, Pakettiautomaatti pa2) {
        super(o, l,pa1,pa2);
        sendClass = 2;
        l.setText("");
        if (o.getHeight() > 20 || o.getWidth() > 50 || o.getDepth() > 60) {
            l.setText("Esine on liian suuri 2. luokan paketille, valitse muu luokka.");
        }

    }
}

class ThirdClass extends Package {

   
    public ThirdClass(NewObject o, Label l,Pakettiautomaatti pa1, Pakettiautomaatti pa2) {
        super(o, l,pa1,pa2);
        sendClass = 3;
        l.setText("");
        if (o.getIfFragile() == true) {
            l.setText("3. Luokka ei sovellu särkyvän esineen kuljetukseen.");
        }
    }

}
