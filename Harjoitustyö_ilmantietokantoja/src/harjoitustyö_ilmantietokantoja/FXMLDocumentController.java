/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö_ilmantietokantoja;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author varpu
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private WebView webWindow;
    @FXML
    private ComboBox<String> automatsComboBox;
    @FXML
    private Button addOnMapBtn;
    @FXML
    private Button createNewPackageBtn;
    @FXML
    private ComboBox<Package> choosePackageCBox;
    @FXML
    private Button sendPackageButton;
    @FXML
    private Button removeButton;
    @FXML
    private Button refreshBtn;
    @FXML
    private TextArea class1Area;
    @FXML
    private TextArea class2Area;
    @FXML
    private TextArea class3Area;
    @FXML
    private Label class1NBlabel;
    @FXML
    private Label class2NBlabel;
    @FXML
    private Label class3NBlabel;
    
    private Stage newPackage;
    private ArrayList<String> automatsList;
   

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            automatsList = new ArrayList<>();

            AutomaattiVerkosto timo = AutomaattiVerkosto.getInstance();
            Storage varasto = Storage.getInstance();

            for (int i = 0; i < timo.getAutomatsArray().size();
                    i++) {
                String automatName = timo.getAutomatsArray().get(i).getName();
                automatsList.add(automatName);

            }

            varasto.getPackageArray().addListener(new ListChangeListener<Package>() {

                @Override
                //täytetään pakettivalikko comboboxi joka kerta kun muutos tapahtuu ObservableList tietorakenteessa, joka on varastoon liitetty
                public void onChanged(ListChangeListener.Change<? extends Package> c) {
                    try {
                        choosePackageCBox.setItems(Storage.getInstance().getPackageArray());
                        choosePackageCBox.setCellFactory(listView -> new PackageListCell());
                        choosePackageCBox.setButtonCell(new PackageListCell());
                        class1Area.setText("");
                        class2Area.setText("");
                        class3Area.setText("");
                        int i1, i2, i3;
                        i1 = 0;
                        i2 = 0;
                        i3 = 0;
                        //lisätään jokainen varastosta löytyvä paketti uudelle välilehdelle oman pakettiluokan kohdalle
                        
                        for (int i = 0; i < Storage.getInstance().getPackageArray().size(); i++) {
                            Package new_package = Storage.getInstance().getPackageArray().get(i);
                            int classNB = Storage.getInstance().getPackageArray().get(i).getSendClass();

                            switch (classNB) {
                                case 1:
                                    i1++;
                                    class1NBlabel.setText("Määrä: " + i1);
                                    class2NBlabel.setText("Määrä: " + i2);
                                    class3NBlabel.setText("Määrä: " + i3);
                                    class1Area.appendText(printPackageInformation(new_package) + "\n\n");
                                    continue;

                                case 2:
                                    i2++;
                                    class1NBlabel.setText("Määrä: " + i1);
                                    class2NBlabel.setText("Määrä: " + i2);
                                    class3NBlabel.setText("Määrä: " + i3);
                                    class2Area.appendText(printPackageInformation(new_package) + "\n\n");
                                    continue;
                                case 3:
                                    i3++;
                                    class1NBlabel.setText("Määrä: " + i1);
                                    class2NBlabel.setText("Määrä: " + i2);
                                    class3NBlabel.setText("Määrä: " + i3);
                                    class3Area.appendText(printPackageInformation(new_package) + "\n\n");
                                    continue;
                                default:
                                    System.out.println("Paketin lähetysluokkaa ei löytynyt");
                                    break;
                            }

                        }

                    } catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            automatsComboBox.getItems().addAll(automatsList);

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

        webWindow.getEngine().load(getClass().getResource("index.html").toExternalForm());
    }

    //luokka jonka avulla liitetään kuva pakettiin, jotka ovat näkyvissä comboboxissa
    class PackageListCell extends ListCell<Package> {

        private final GridPane gridPane = new GridPane();
        private final ImageView packageImage = new ImageView();
        private final Label packageLabel = new Label();
        private final AnchorPane content = new AnchorPane();

        public PackageListCell() {
            packageImage.setFitWidth(80);
            packageImage.setPreserveRatio(true);
            GridPane.setConstraints(packageImage, 0, 0, 1, 3);
            packageLabel.setStyle("-fx-font-size: 14");
            GridPane.setConstraints(packageLabel, 3, 0);

            gridPane.getChildren().setAll(packageImage, packageLabel);
            AnchorPane.setTopAnchor(gridPane, 0d);
            AnchorPane.setLeftAnchor(gridPane, 0d);
            AnchorPane.setBottomAnchor(gridPane, 0d);
            AnchorPane.setRightAnchor(gridPane, 0d);
            content.getChildren().add(gridPane);
        }

        @Override
        protected void updateItem(Package item, boolean empty) {
            super.updateItem(item, empty);
            setGraphic(null);
            setText(null);
            if (item == null || empty) {
                setText(null);
                setGraphic(null);
            } else {

                packageLabel.setText(item.getName());

                switch (item.getSendClass()) {
                    case 1:
                        packageImage.setImage(new Image("image1.png"));
                        break;
                    case 2:
                        packageImage.setImage(new Image("image2.png"));
                        break;
                    case 3:
                        packageImage.setImage(new Image("image3.png"));
                        break;

                }
                setGraphic(content);
                setText(null);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

            }

        }
    }

    //lisätään automaatti kartalle javascript rajapintaa hyödyntäen
    @FXML
    private void addOnMap(ActionEvent event) throws IOException {
        Pakettiautomaatti automat;
        AutomaattiVerkosto timo = AutomaattiVerkosto.getInstance();
        ArrayList<Pakettiautomaatti> listOfAutomats = new ArrayList<>();
        listOfAutomats = timo.getAutomatsArray();
        for (int i = 0; i < listOfAutomats.size(); i++) {
            if (listOfAutomats.get(i).getName().equals(automatsComboBox.getValue())) {
                automat = listOfAutomats.get(i);
                String automatAddress = automat.getAddress() + "," + automat.getCode() + automat.getCity();
                String automatInfo = automat.getName() + automat.getName();
                webWindow.getEngine().executeScript("document.goToLocation('" + automatAddress + "','" + automatInfo + "','red')");
            }
        }

    }

    //avataan ikkuna uuden paketin luomiseen
    @FXML
    private void createNewPackage(ActionEvent event) throws IOException {

        newPackage = new Stage();
        Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageCreating.fxml"));
        Scene scene = new Scene(page);
        newPackage.setScene(scene);
        scene.getStylesheets().add(FXMLDocumentController.class.getResource("styleSheet.css").toExternalForm());
        newPackage.show();

    }

    //piirettään reitit kartalle
    @FXML
    private void sendPackage(ActionEvent event) throws IOException {
        Storage varasto = Storage.getInstance();
        ArrayList<String> latLonList;
        latLonList = new ArrayList<>();
        float lat1, lat2, lon1, lon2;
        int classNb;
        classNb = 0;

        for (int i = 0; i < varasto.getPackageArray().size(); i++) {

            if (varasto.getPackageArray().get(i).getName().equals(choosePackageCBox.getValue().getName())) {
                lat1 = varasto.getPackageArray().get(i).getStartautomat().getGeoPoint().getLattitude();
                latLonList.add(Float.toString(lat1));
                lon1 = varasto.getPackageArray().get(i).getStartautomat().getGeoPoint().getLongitude();
                latLonList.add(Float.toString(lon1));
                lat2 = varasto.getPackageArray().get(i).getEndautomat().getGeoPoint().getLattitude();
                latLonList.add(Float.toString(lat2));
                lon2 = varasto.getPackageArray().get(i).getEndautomat().getGeoPoint().getLongitude();
                latLonList.add(Float.toString(lon2));
                classNb = varasto.getPackageArray().get(i).getSendClass();
                varasto.getPackageArray().remove(i);

            }

        }
        webWindow.getEngine().executeScript("document.createPath(" + latLonList + ",'red'," + classNb + ")");

    }

    //pyyhitään kaikki kartalta löytyvät reitit javascript rajapintaa hyödyntäen
    @FXML
    private void deleteTraject(ActionEvent event) {
        webWindow.getEngine().executeScript("document.deletePaths()");

    }

    //metodilla haetaan pakettejen tiedot
    public String printPackageInformation(Package p) {
        String packageName = p.getName();
        Pakettiautomaatti startAutomat = p.getStartautomat();
        Pakettiautomaatti endAutomat = p.getEndautomat();
        double distance = p.getDistance();
        return p.getName() + " joka sisältää esineen: " + p.getObject().getName() + " lähtee automaatilta: " + startAutomat + " automaatille: " + endAutomat + " ja matkustaa " + Math.round(distance) + "kilometriä.";
    }

}
