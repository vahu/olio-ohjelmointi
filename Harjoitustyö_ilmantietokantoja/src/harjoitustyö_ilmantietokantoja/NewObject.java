/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö_ilmantietokantoja;




/**
 *
 * @author varpu
 */
public final class NewObject {

    
    private String name, size;
    private double weight;
    private boolean fragile;
    private double height, width, depth;

    public NewObject(String n, String s, double w, boolean f) {
        name = n;
        size = s;
        weight = w;
        fragile = f;
        //seuraavaksi määritetään esineen korkeus, leveys ja syvyys erottamalla eri arvot merkkijonosta
        String[] parts;
        parts = size.split("\\*");
        height = Double.parseDouble(parts[0]);
        width = Double.parseDouble(parts[1]);
        depth = Double.parseDouble(parts[2]);
        
        

    }   

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public boolean getIfFragile() {
        return fragile;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    public double getDepth() {
        return depth;
    }
    
    public String toString(){
        return "Esine nimeltä: "+name;
    }

}
