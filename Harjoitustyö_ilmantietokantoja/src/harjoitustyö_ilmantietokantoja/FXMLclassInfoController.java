/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö_ilmantietokantoja;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author varpu
 */
public class FXMLclassInfoController implements Initializable {

    @FXML
    private Button okButton;
    @FXML
    private TextArea textArea;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //haetaan luokkatiedot erilliestä tiedostosta 
        String filename = "classInfo.txt";
        try {
            BufferedReader in = new BufferedReader (new FileReader(filename));
            String line; 
            while ((line=in.readLine()) != null){
                textArea.setText(textArea.getText()+"\n"+line);
            }
        } catch (FileNotFoundException ex) {
            System.err.println("File not found");
        } catch (IOException ex) {
           System.err.println("IOException.");
        }
        
    }    

    //painamalla "ok" nappia, käyttäjä osoittaa, että on lukenut tiedot ja ikkuna suljetaan 
    // seuraavan funktion avulla
    @FXML
    private void closeWindow(ActionEvent event) {
        Stage scene = (Stage) okButton.getScene().getWindow();
        scene.close();
    }
    
}
