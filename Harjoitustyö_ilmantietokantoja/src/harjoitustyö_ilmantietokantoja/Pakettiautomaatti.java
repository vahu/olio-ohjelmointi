package harjoitustyö_ilmantietokantoja;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author varpu
 */


class GeoPoint {
    private float lattitude;
    private float longitude;

    public GeoPoint(float lat, float lng){
        lattitude = lat;
        longitude = lng;
    }
    
     public float getLattitude(){
        return lattitude;
    }
    
    public float getLongitude(){
        return longitude;
    }
    public String toString(){
        return "lattitude: "+ Float.toString(lattitude)+"longitude: " + Float.toString(longitude);
    } 
    
}
public class Pakettiautomaatti {

    private String code;
    private String city;
    private String address;
    private String openHours;
    private String name;
    private GeoPoint geoPoint;
    

    public String getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getOpenHours() {
        return openHours;
    }

    public String getName() {
        return name;
    }
    
    public GeoPoint getGeoPoint(){
        return geoPoint;
    }
    
    public String toString(){
        return name;
    }
   

    public Pakettiautomaatti(String c, String ci, String a, String oh, String n, GeoPoint gp) {
        code = c;
        city = ci;
        address = a;
        openHours = oh;
        name = n;
        geoPoint = gp;
       
                
    }
    


}
