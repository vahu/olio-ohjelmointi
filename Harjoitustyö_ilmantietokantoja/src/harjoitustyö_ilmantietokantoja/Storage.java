/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö_ilmantietokantoja;

import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author varpu
 */
public final class Storage {
    private ObservableList<Package> package_array; 
        private static Storage instance = null;
    
    private Storage(){
        package_array = FXCollections.observableArrayList();
    }
    
   
    
    public ObservableList<Package> getPackageArray(){
        return package_array;
    }
    
     public final static Storage getInstance() throws IOException {
        if (instance == null) {
            instance = new Storage();
        }
        return instance;
    }
     
    
}
