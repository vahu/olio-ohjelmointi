/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö_ilmantietokantoja;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author varpu
 */
public class FXMLPackageCreatingController implements Initializable {

    @FXML
    private ComboBox<String> objectComboBox;
    @FXML
    private TextField objectNameField;
    @FXML
    private TextField objectSizeField;
    @FXML
    private TextField objectWeightField;
    @FXML
    private CheckBox fragileCheckBox;
    @FXML
    private ComboBox<String> departureCityComboBox;
    @FXML
    private ComboBox<String> departureAutomatComboBx;
    @FXML
    private ComboBox<String> arrivalCityComboBox;
    @FXML
    private ComboBox<String> arrivalAutomatComboBox;
    @FXML
    private RadioButton class3RadioButton;
    @FXML
    private RadioButton class2RadioBtn;
    @FXML
    private RadioButton class1RadioBtn;
    @FXML
    private Button cancelButton;
    @FXML
    private Button createPackageButton;
    @FXML
    private Label class1ErrorLabel;
    @FXML
    private Label class2ErrorLabel;
    @FXML
    private Label class3ErrorLabel;
    @FXML
    private Label globalErrorLabel;
    
    private Stage classInfo;
    private ArrayList<NewObject> objectArray;
    private ArrayList<String> cityList;
    private ArrayList<String> automatList;
    private ArrayList<String> objectNameList;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //lisätään neljä esinettä tietorakenteeseen
        objectArray = new ArrayList<>();
        NewObject Object1 = new NewObject("Muovinen Kahvikuppi", "10*10*15", 0.8, false);
        objectArray.add(Object1);
        NewObject Object2 = new NewObject("Lohikäärme esine", "19*25*20", 1.5, true);
        objectArray.add(Object2);
        NewObject Object3 = new NewObject("Kanettava tietokone", "3*30*25", 0.8, true);
        objectArray.add(Object3);
        NewObject Object4 = new NewObject("Tuoli", "100*50*50", 3, false);
        objectArray.add(Object4);

        //alustetaan esine ComboBoxi antamalla sille lista, johon on talennettu jokaisen esineen nimi. 
        objectNameList = new ArrayList<>();
        

        for (int i = 0; i < objectArray.size();
                i++) {
            String objectName = objectArray.get(i).getName();
            objectNameList.add(objectName);
        }
        objectComboBox.getItems().addAll(objectNameList);

        //alustetaan pakettiautomaatti comboboxit tallentamalla jokaisen automaatin nimet
        // erilliseen ArrayListaan 
        try {
            cityList = new ArrayList<>();
            AutomaattiVerkosto timo = AutomaattiVerkosto.getInstance();
            for (int i = 0; i < timo.getAutomatsArray().size(); i++) {
                String new_city = timo.getAutomatsArray().get(i).getCity();
                cityList.add(new_city);
            }
            Set set = new HashSet();
            set.addAll(cityList);
            ArrayList distinctCityList = new ArrayList(set);
            departureCityComboBox.getItems().addAll(distinctCityList);
            arrivalCityComboBox.getItems().addAll(distinctCityList);

        } catch (IOException ex) {
            Logger.getLogger(FXMLPackageCreatingController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    //valitaan lähtöautomaatti, pakettiautomaatin nimet päivittyvät valitun kaupungin mukaan
    private void selectDepartureAutomat(ActionEvent event) throws IOException {
        departureAutomatComboBx.getItems().clear();
        String departureCity = departureCityComboBox.getValue();
        automatList = new ArrayList<>();
        AutomaattiVerkosto timo = AutomaattiVerkosto.getInstance();
        try {
            timo = AutomaattiVerkosto.getInstance();
            for (int i = 0; i < timo.getAutomatsArray().size(); i++) {
                String new_city = timo.getAutomatsArray().get(i).getCity();
                if (departureCity.equals(new_city)) {
                    String new_automat = timo.getAutomatsArray().get(i).getName();
                    automatList.add(new_automat);
                   
                }

            }

            departureAutomatComboBx.getItems().addAll(automatList);
        } catch (IOException ex) {
            Logger.getLogger(FXMLPackageCreatingController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    //metodi, jolla valitaan saapumisautomaatti comboboxista, pakettiautomaatin nimet päivittyvät kaupungin mukaan
    private void selectArrivalAutomat(ActionEvent event
    ) throws IOException {
        arrivalAutomatComboBox.getItems().clear();
        String arrivalCity = arrivalCityComboBox.getValue();
        automatList = new ArrayList<>();
        AutomaattiVerkosto timo = AutomaattiVerkosto.getInstance();
        try {
            timo = AutomaattiVerkosto.getInstance();
            for (int i = 0; i < timo.getAutomatsArray().size(); i++) {
                String new_city = timo.getAutomatsArray().get(i).getCity();
                if (arrivalCity.equals(new_city)) {
                    String new_automat = timo.getAutomatsArray().get(i).getName();
                    automatList.add(new_automat);
                }

            }
            arrivalAutomatComboBox.getItems().addAll(automatList);
        } catch (IOException ex) {
            Logger.getLogger(FXMLPackageCreatingController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    //luokkatietojen näyttäminen käyttäjälle tapahtuu avaamalla uusi ikkuna seuraavasti
    private void giveClassInfo(ActionEvent event) {
        try {
            classInfo = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLclassInfo.fxml"));
            Scene scene = new Scene(page);
            classInfo.setScene(scene);
            scene.getStylesheets().add(FXMLPackageCreatingController.class.getResource("styleSheet.css").toExternalForm());
            classInfo.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLPackageCreatingController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    @FXML
    private void cancelPackageCreating(ActionEvent event) {
        closeWindow(cancelButton);
    }

    //metodi, jolla ikkuna suljetaan painamalla tiettyä nappulaa 
    private void closeWindow(Button button) {
        Stage scene = (Stage) button.getScene().getWindow();
        scene.close();
    }

    //metodilla lisätään uusi paketti varastoon. Metodi ottaa parametreiksi uuden paketin, ja pakettiluokkaan liittyvän virheotsikon 
    //otsikko ei sisällä tekstiä kun paketti on luoto
    private void addInStorage(Label label, Package new_package) throws IOException {
        Storage storage = Storage.getInstance();
        if (label.getText().equals("")) {
            ObservableList<Package> packageArray = storage.getPackageArray();
            packageArray.add(new_package);
            System.out.println("Esineen lisääminen onnistui");
            closeWindow(createPackageButton);
            for (int i = 0; i < storage.getPackageArray().size(); i++) {
                System.out.println(storage.getPackageArray().get(i));
            }
        } else {
            System.out.println("Tapahtui virhe");
        }

    }

//metodilla etsitään tietty esine kun on tiedossa vain esineen nimi
    private NewObject searchObjectByName(String objectName) {
        NewObject objectFound = null;
        for (int i = 0; i < objectArray.size(); i++) {
            if (objectArray.get(i).getName().equals(objectName)) {
                objectFound = objectArray.get(i);
            }
        }
        return objectFound;
    }

    @FXML
    private void createNewPackage(ActionEvent event) throws IOException {
        if (departureAutomatComboBx.getValue() == null || arrivalAutomatComboBox.getValue() == null) {
            globalErrorLabel.setText("Pakettia ei voida luoda, tarkista automaattivalinta!");
        } 
        else if(!class1RadioBtn.isSelected() && !class2RadioBtn.isSelected() && !class3RadioButton.isSelected()){
            globalErrorLabel.setText("Unohdit luokan valinnan!");
        }
        else {
            globalErrorLabel.setText("");
            Pakettiautomaatti startAutomat;
            startAutomat = searchForAutomat(departureAutomatComboBx.getValue());
            Pakettiautomaatti endAutomat;
            endAutomat = searchForAutomat(arrivalAutomatComboBox.getValue());

            if (class1RadioBtn.isSelected()) {
                if (objectComboBox.getValue() == null) {
                    if (objectNameField.getText().isEmpty() || objectSizeField.getText().isEmpty() || objectWeightField.getText().isEmpty()) {
                        globalErrorLabel.setText("Pakettia ei voida luoda, tarkista syötteet!");
                    } else {
                        globalErrorLabel.setText("");
                        NewObject new_object = new NewObject(objectNameField.getText(), objectSizeField.getText(), Double.parseDouble(objectWeightField.getText()), fragileCheckBox.isSelected());

                        Package new_Class1 = new FirstClass(new_object, class1ErrorLabel, startAutomat, endAutomat);
                        addInStorage(class1ErrorLabel, new_Class1);
                    }
                } else {
                    globalErrorLabel.setText("");
                    NewObject new_object = searchObjectByName(objectComboBox.getValue());
                    Package new_Class1 = new FirstClass(new_object, class1ErrorLabel, startAutomat, endAutomat);
                    addInStorage(class1ErrorLabel, new_Class1);
                }

            } else if (class2RadioBtn.isSelected()) {
                if (objectComboBox.getValue() == null) {
                    if (objectNameField.getText().isEmpty() || objectSizeField.getText().isEmpty() || objectWeightField.getText().isEmpty()) {
                        globalErrorLabel.setText("Pakettia ei voida luoda, tarkista syötteet!");
                    } else {
                        globalErrorLabel.setText("");
                        NewObject new_object = new NewObject(objectNameField.getText(), objectSizeField.getText(), Double.parseDouble(objectWeightField.getText()), fragileCheckBox.isSelected());
                        Package new_Class2 = new SecondClass(new_object, class2ErrorLabel, startAutomat, endAutomat);
                        addInStorage(class2ErrorLabel, new_Class2);
                    }

                } else {
                    NewObject new_object = searchObjectByName(objectComboBox.getValue());
                    Package new_Class2 = new SecondClass(new_object, class2ErrorLabel, startAutomat, endAutomat);
                    addInStorage(class2ErrorLabel, new_Class2);

                }

            } else if (class3RadioButton.isSelected()) {
                if (objectComboBox.getValue() == null) {
                    if (objectNameField.getText().isEmpty() || objectSizeField.getText().isEmpty() || objectWeightField.getText().isEmpty()) {
                        globalErrorLabel.setText("Pakettia ei voida luoda, tarkista syötteet!");
                    } else {
                        globalErrorLabel.setText("");
                        NewObject new_object = new NewObject(objectNameField.getText(), objectSizeField.getText(), Double.parseDouble(objectWeightField.getText()), fragileCheckBox.isSelected());
                        Package new_Class3 = new ThirdClass(new_object, class3ErrorLabel, startAutomat, endAutomat);
                        addInStorage(class3ErrorLabel, new_Class3);
                    }
                } else {
                    globalErrorLabel.setText("");
                    NewObject new_object = searchObjectByName(objectComboBox.getValue());
                    Package new_Class3 = new ThirdClass(new_object, class3ErrorLabel, startAutomat, endAutomat);
                    addInStorage(class3ErrorLabel, new_Class3);

                }
            }
        }
    }

    //metodin avulla etsitään automaatti nimellä, jonka käyttäjä on valinnut
    private Pakettiautomaatti searchForAutomat(String automatName) throws IOException {
        AutomaattiVerkosto timo = AutomaattiVerkosto.getInstance();
        Pakettiautomaatti automat = null;
        for (int i = 0; i < timo.getAutomatsArray().size(); i++) {
            if (timo.getAutomatsArray().get(i).getName().equals(automatName)) {
                automat = timo.getAutomatsArray().get(i);
                return automat;
            }
        }
        return automat;
    }
}
