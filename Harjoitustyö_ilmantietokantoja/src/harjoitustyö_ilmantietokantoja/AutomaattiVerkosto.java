/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyö_ilmantietokantoja;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author varpu
 */
public final class AutomaattiVerkosto {

    private Document doc;
    private ArrayList<Pakettiautomaatti> automats_Array;
    private static AutomaattiVerkosto instance = null;


    public ArrayList<Pakettiautomaatti> getAutomatsArray() {
        return automats_Array;
    }

   

    private AutomaattiVerkosto() throws MalformedURLException, IOException {
        automats_Array = new ArrayList<>();
        
        //alla olevan koodin ansiosta käydään linkistä löytyvä xml-tiedosto läpi, ja haetaan halutut tiedot
        
        URL url = new URL("http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String content = "";
            String line;
            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }
            br.close();

            try {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                doc = dBuilder.parse(new InputSource(new StringReader(content)));
                doc.getDocumentElement().normalize();
                getAutomatsData();
            } catch (ParserConfigurationException | SAXException ex) {
                Logger.getLogger(AutomaattiVerkosto.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    /*Seuraavalla metodilla etsitään pakettiautomaatin määrävät tiedot xml-dokumentistä
    ja lisätään pakettiautomaatit tietoineen tietorakenteeseen, joka on tässä tapauksessa ArrayList*/

    private void getAutomatsData() {
        NodeList nodes = doc.getElementsByTagName("item");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            GeoPoint new_GeoPoint = new GeoPoint(Float.parseFloat(getValue("lat", e)), Float.parseFloat(getValue("lng", e)));
            Pakettiautomaatti new_automat = new Pakettiautomaatti(getValue("postalcode", e), getValue("city", e),
                    getValue("address", e), getValue("availability", e), getValue("name", e),new_GeoPoint);
            automats_Array.add(new_automat);

            
            
        }

    }

    /*Metodilla haetaan tiedot xml-tiedostosta, kertomalla mistä kohtaa tiedot haluaa
    tarkalleen*/
    private String getValue(String tag, Element e) {
        return e.getElementsByTagName(tag).item(0).getTextContent();
    }

    public final static AutomaattiVerkosto getInstance() throws IOException {
        if (instance == null) {
            instance = new AutomaattiVerkosto();
        }
        return instance;
    }

}
