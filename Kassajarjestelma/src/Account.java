public abstract class Account {
	private String tilinro;
	private int money, credit;

	public Account(String nb, int m, int c) {
		tilinro = nb;
		money = m;
		credit = c;

	}

	public void addMoney(int raham) {
		money += raham;

	}

	public void withdrawMoney(int raham) {
		if (money - raham < credit) {
			System.out.println("Ei riittävästi rahaa.");
		} else {
			money -= raham;
		}

	}

	public String getAccountnb() {
		return tilinro;
	}

	public void print(String tili) {
		if (credit != 0) {
			System.out.println("Tilinumero: " + tili + " Tilillä rahaa: "
					+ money + "Luottoraja: " + credit);
		} else {
			System.out.println("Tilinumero: " + tili + " Tilillä rahaa: "
					+ money);
		}

	}

}

class SimpleAccount extends Account {

	public SimpleAccount(String nb, int m) {
		super(nb, m, 0);

	}
}

class CreditAccount extends Account {

	public CreditAccount(String nb, int m, int c) {
		super(nb, m, c);
	}
}
