import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Mainclass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank pankki = new Bank();
		Scanner choice = new Scanner(new BufferedReader(new InputStreamReader(
				System.in)));
		String tilinro;
		int raha;
		int luotto;
		while (true) {
			System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
			System.out
					.println("1) Lisää tavallinen tili\n2) Lisää luotollinen tili");
			System.out.println("3) Tallenna tilille rahaa\n4) Nosta tililtä");
			System.out
					.println("5) Poista tili\n6) Tulosta tili\n7) Tulosta kaikki tilit\n0) Lopeta");
			System.out.print("Valintasi: ");
			int valinta = choice.nextInt();
			switch (valinta) {
			case 1:
				
				System.out.print("Syötä tilinumero: ");
				tilinro = choice.next();
				System.out.print("Syötä rahamäärä: ");
				raha = choice.nextInt();
				pankki.AddAccount(tilinro, raha);
				break;
			case 2:
				System.out.print("Syötä tilinumero: ");
				tilinro = choice.next();
				System.out.print("Syötä rahamäärä: ");
				raha = choice.nextInt();
				System.out.print("Syötä luottoraja: ");
				luotto = choice.nextInt();
				pankki.AddCreditAccount(tilinro,raha,luotto);
				break;
			case 3:
				System.out.print("Syötä tilinumero: ");
				tilinro = choice.next();
				System.out.print("Syötä rahamäärä: ");
				raha = choice.nextInt();
				pankki.AddMoney(tilinro, raha);
				break;
			case 4:
				System.out.print("Syötä tilinumero: ");
				tilinro = choice.next();
				System.out.print("Syötä rahamäärä: ");
				raha = choice.nextInt();
				pankki.TakeMoney(tilinro, raha);
				break;
			case 5:
				System.out.print("Syötä poistettava tilinumero: ");
				tilinro = choice.next();
				pankki.DeleteAccount(tilinro);
				break;
			case 6:
				System.out.print("Syötä tulostettava tilinumero: ");
				tilinro = choice.next();
				pankki.printAccount(tilinro);
				break;
			case 7:
				pankki.printall();
				break;
			case 0:
				return;
			default:
				System.out.println("Valinta ei kelpaa.");
			}
		} 
	}

}
