import java.util.ArrayList;
//import java.util.Iterator;

public class Bank {
	private ArrayList<Account> account_array;

	
	public Bank(){
		account_array = new ArrayList<>();	
	}

	public void AddAccount(String tilinro, int raha ) {
		Account a = new SimpleAccount(tilinro,raha);
		account_array.add(a);
		System.out.println("Tili luotu.");
		//System.out.println("Pankkiin lisätään: "+tilinro+","+raha);
		}

public void AddCreditAccount(String tilinro, int raha, int credit){
	Account a = new CreditAccount(tilinro,raha,credit);
	account_array.add(a);
	System.out.println("Tili luotu.");
	//System.out.println("Pankkiin lisätään: "+tilinro+","+raha+","+credit);
}
	public void DeleteAccount(String tilinro) {
		for (int i=0;i<account_array.size();i++){
			if (account_array.get(i).getAccountnb().equals(tilinro)){
				account_array.remove(i);
				System.out.println("Tili poistettu.");
				break;
			}
		
		}
		
	}

	public void printAccount(String tilinro){
		//System.out.println("Etsitään tiliä: "+tilinro);
		for(int i=0;i<account_array.size();i++){
				if(account_array.get(i).getAccountnb().equals(tilinro)){
				account_array.get(i).print(tilinro);
				break;
			}
		}
		
	}
	
	public void printall(){
		System.out.println("Kaikki tilit:");
		for (int i=0;i<account_array.size();i++){
			String tilinb = account_array.get(i).getAccountnb();
			account_array.get(i).print(tilinb);
			
		}
	}
	
	
	public void AddMoney(String tilinro, int raha){
		for(int i=0;i<account_array.size();i++){
			if (account_array.get(i).getAccountnb().equals(tilinro)){
				account_array.get(i).addMoney(raha);
				//System.out.println("Talletetaan tilille: "+tilinro+" rahaa "+raha);
			}
		}
	}
	
	public void TakeMoney(String tilinro, int raha){
		for(int i=0;i<account_array.size();i++){
			if(account_array.get(i).getAccountnb().equals(tilinro)){
				account_array.get(i).withdrawMoney(raha);
				//System.out.println("Nostetaan tililtä: "+tilinro+ " rahaa "+raha);
			}
		}
		
	}
}



	
