import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class ReadAndWriteIO {
	private String filename;
	private String outfile;
	public ReadAndWriteIO (String s, String o){
		filename = s;
		outfile = o; 
	}
	
	
	public void readAndWrite() throws IOException{
		BufferedReader in = new BufferedReader(new FileReader(filename));
		String s;
		BufferedWriter out = new BufferedWriter(new FileWriter(outfile));
		while ((s = in.readLine()) != null){
			if (s.length()<30 && !s.trim().isEmpty() && s.indexOf('v')!=-1){
				out.write(s+"\n");
			}
			else{
				continue;
			}
			
		}
		in.close();
		out.close();
	}

}
