/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kartta.vko11;

import javafx.scene.shape.Line;

/**
 *
 * @author varpu
 */
public class NewLine {
   private String name;
   private Line new_line;
   private static int i=1;
   
   public NewLine(Line l){
        String index = Integer.toString(i++);
        name = "Viiva"+ index;
        new_line = l;
    }
   
   public String toString(){
       return "Viiva "+name+ " koordinaateilla "+new_line;
   }
    
}
