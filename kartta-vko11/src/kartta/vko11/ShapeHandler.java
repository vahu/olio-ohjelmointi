/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kartta.vko11;

import java.util.ArrayList;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;

/**
 *
 * @author varpu
 */
public class ShapeHandler {

    public ArrayList<point> circle_array;
    public ArrayList<NewLine> line_array;
    private static ShapeHandler instance = null;

    private ShapeHandler() {

        circle_array = new ArrayList<>();
        line_array = new ArrayList<>();
        /*point piste = new point();
        shape_array.add(piste);*/

    }

    public ArrayList<point> getShapeArray() {
        return circle_array;
    }

    public ArrayList<NewLine> getLineArray() {
        return line_array;
    }

    public final static ShapeHandler getInstance() {
        if (instance == null) {
            instance = new ShapeHandler();
        }
        return instance;
    }
    private double startX = -1, startY = -1;
    private Line line = null;

    public void drawLine(double x, double y, AnchorPane pane) {

        if (startX == -1 && startY == -1) {
            startX = x;
            startY = y;
            return;
        } else {
            //pane.getChildren().remove(line);
            line = new Line();
            line.setStartX(startX);
            line.setStartY(startY);
            line.setEndX(x);
            line.setEndY(y);
            //tarkistetaan, ovatko viivat lisätty tietorakenteeseen
            
              NewLine new_line = new NewLine(line);
            line_array.add(new_line);
            for (int i = 0; i < line_array.size(); i++) {
                System.out.println(line_array.get(i));
            }
            pane.getChildren().add(line);
            startX = -1;
            startY = -1;
            

        }

    }

}
