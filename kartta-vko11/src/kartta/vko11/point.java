/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kartta.vko11;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author varpu
 */
public class point {
    private  String name;
    private  Circle point;
    private static int i = 1;
    
    public point(double x, double y){
        String index = Integer.toString(i++);
        name = "piste"+ index;
        point = createPoint(x,y);
    }
    
    public Circle getCircle(){
        return point;
    }
    
    public String getName(){
        return name;
    }
    private static Circle createPoint(double x,double y){
        Circle c = new Circle();
        c.setCenterX(x);
        c.setCenterY(y);
        c.setRadius(7);
        c.setFill(Color.FIREBRICK);
        return c;
    }
    
    @Override
     public String toString(){
       return "Piste " + name + " kohdassa " + point; 
    }
}
