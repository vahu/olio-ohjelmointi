/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kartta.vko11;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author varpu
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private AnchorPane pane;

    

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // TODO
    }

    @FXML
    private void drawCircle(MouseEvent event) {
        double xvalue = event.getX();
        double yvalue = event.getY();

        point p = new point(xvalue, yvalue);

        Circle piste = p.getCircle();
        ShapeHandler sh = ShapeHandler.getInstance();
        sh.getShapeArray().add(p);
        //tarkistetaan, ovatko pisteet lisätty listaan
        for (int i = 0; i < sh.getShapeArray().size(); i++) {
            System.out.println(sh.getShapeArray().get(i));
        }

        piste.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                System.out.println("Hei, olen piste!");
                sh.drawLine(event.getX(), event.getY(), pane);
                event.consume();

            }

        });

        pane.getChildren()
                .add(piste);

    }

}
