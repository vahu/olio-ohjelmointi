import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Mainclass {
    
    public static void main(String[] args) {
        Scanner s = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
        
        System.out.print("Anna koiralle nimi: ");
        String name = s.nextLine();
        Dog k = new Dog(name);
        System.out.print("Mitä koira sanoo: ");
        String says = s.nextLine();
		
        k.speak(says);
        
    }
    
}
