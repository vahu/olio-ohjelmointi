import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Dog {
    
    private String name;
    private String says;
    
    public Dog(String n) {
        if(!n.trim().isEmpty()) {
            name = n;
            System.out.println("Hei, nimeni on " + n);
        }
        else {
            name = "Doge";
            System.out.println("Hei, nimeni on " + name);
            }
        says = "Much wow!";
        
    }

    public void speak(String s) {
		Scanner sc = new Scanner(s);
        while(sc.hasNext()) {
        	
            if (sc.hasNextBoolean()){
            	System.out.println("Such boolean: "+sc.nextBoolean());
        	}
            else if (sc.hasNextInt()){
            	System.out.println("Such integer: " + sc.nextInt());
            }
            else{
            	System.out.println(sc.next());
            }
		
    }
        sc.close();
}
}