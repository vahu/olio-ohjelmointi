/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package verkkoselain.vko10;

import java.net.URL;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

/**
 *
 * @author varpu
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TextField searchField;
    @FXML
    private WebView webWindow;
    @FXML
    private Button refreshButton;

    private String url;
    @FXML
    private Button shoutButton;
    @FXML
    private Button initializeButton;
    @FXML
    private Button previousPageBtn;
    @FXML
    private Button nextPageBtn;

    private ArrayList<String> url_List;
    
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        url_List = new ArrayList<>();
        
        // TODO
    }

    @FXML
    private void searchOnWeb(ActionEvent event) {
        url = searchField.getText();
        
        
        if (url.contains("index.html")) {
            webWindow.getEngine().load(getClass().getResource("index.html").toExternalForm());
            searchField.setText("http://");
        } else {
            url_List.add(url);
            webWindow.getEngine().load(url);
            searchField.setText("http://");
            

        }
    }

    @FXML
    private void refreshPage(ActionEvent event) {
        url = searchField.getText();
        webWindow.getEngine().load(url);

    }

    @FXML
    private void shoutPressed(ActionEvent event) {
        webWindow.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void initalizePressed(ActionEvent event) {
        webWindow.getEngine().executeScript("initialize()");
    }

    @FXML
    private void chargePreviousPage(ActionEvent event) {
      ListIterator itr = url_List.listIterator(url_List.size());
      String url_now = searchField.getText();
      while (itr.hasPrevious()){
          String current_address = (String) itr.previous();
          if(current_address.contains(url_now)){
              current_address = (String) itr.previous();
              webWindow.getEngine().load(current_address);
              searchField.setText(current_address);
              return;
              
          }
      }
     
    }

    @FXML
    private void chargeNextPage(ActionEvent event) {
        String url_now = searchField.getText();
        ListIterator itr = url_List.listIterator();
        while (itr.hasNext()){
            String current_url = (String)itr.next();
            if (current_url.contains(url_now)){
                current_url = (String)itr.next();
                webWindow.getEngine().load(current_url);
                searchField.setText(current_url);
                return;
            }
            
            
        }
        
        
    }

   

}
