package pulloautomaatti.vko8;

import java.util.ArrayList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public final class BottleDispenser {

    private int bottles;
    private int money;
    private ArrayList<Bottle> bottle_array;
    private static BottleDispenser instance = null;

    private BottleDispenser() {
        bottles = 5;
        money = 0;
        for (int i = 0; i < bottles; i++) {
            bottle_array = new ArrayList<Bottle>();
            Bottle new_bottle = new Bottle("Vesi", "Vesituotto", 0.0, 0.5, 1);
            bottle_array.add(new_bottle);
            new_bottle = new Bottle("Fanta", "Coca-cola", 0.2, 0.5, 2.5);
            bottle_array.add(new_bottle);
            new_bottle = new Bottle("Fanta", "Coca-cola", 0.1, 0.33, 1.5);
            bottle_array.add(new_bottle);
            new_bottle = new Bottle("Cola", "Coca-cola", 0.3, 0.5, 2.5);
            bottle_array.add(new_bottle);
            new_bottle = new Bottle("EnergyDrink", "RedBull", 0.4, 0.5, 3);
            bottle_array.add(new_bottle);
            new_bottle = new Bottle("EnergyDrink", "RedBull", 0.4, 0.25, 2);
            bottle_array.add(new_bottle);
        }
        
    }
    
    public int getBottles(){
        return bottles;
    }
    public int getMoney(){
        return money;
    }
    public ArrayList<Bottle> getBottleArray(){
        return bottle_array;
    }

    public final static BottleDispenser getInstance() {
        if (instance == null) {

            instance = new BottleDispenser();

        }
        return instance;
    }
    
    public void buyBottle(TextField tf, ComboBox<String> drinkChoiceBox, ComboBox<Double> sizeChoiceBox, Label l){
        BottleDispenser bd = BottleDispenser.getInstance();
      

        for (int i = 0; i < bottle_array.size(); i++) {
            if (bottle_array.get(i).getName().equals(drinkChoiceBox.getValue())
                    && bottle_array.get(i).getSize() == sizeChoiceBox.getValue()) {
                int n = bottle_array.size() - 1;
                Bottle b = bd.getBottleArray().get(n);
                if (n <= 0) {
                    tf.setText("Juomat ovat loppuneet.");
                } else if (bd.getMoney() <= b.getPrice()) {
                    tf.setText("Syötä rahaa ensin!");
                } else {
                    money -= b.getPrice();
                    removeBottle(i);
                    l.setText("Rahaa laitteessa: " + money);
                    tf.setText("Ota pullo");
                    
                }
                break;
                
            }
            else{
                tf.setText("Pulloa ei ole saatavilla!"); 
                        }
        }

    }

    public int removeBottle(int i) {
        //int n= bottle_array.size()-1;
        bottle_array.remove(i);
        return bottle_array.size();
    }

    public void addMoney(double m) {
        money += m;

    }

    public void returnMoney(TextField tf) {
        tf.setText("Rahaa palautettu: "+money);
        money = 0;
    }

    public void tulostus() {
        bottles = bottle_array.size();
        for (int i = 0; i < bottles; i++) {
            Bottle b = bottle_array.get(i);
            System.out.println(i + 1 + "." + "Nimi:" + b.getName() + "\n" + "Koko: " + b.getSize() + " Hinta: " + b.getPrice());
        }

    }
}
