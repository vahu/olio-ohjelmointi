/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pulloautomaatti.vko8;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author varpu
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Button boughtbottle;
    @FXML
    private Button rahapalautus;
    @FXML
    private TextField textfield;
    @FXML
    private Slider slider;
    @FXML
    private Label moneyQuantity;
    @FXML
    private Button drinkChoiceButton;
    @FXML
    private Button sizeChoiceButton;
    @FXML
    private Label limsaTitle;
    @FXML
    private Label sizeTitle;
    @FXML
    private ComboBox<String> drinkChoiceBox;
    @FXML
    private ComboBox<Double> sizeChoiceBox;
   @FXML
    private Button receipt;

    public FXMLDocumentController() {
    }
    private ArrayList<String> juomat;
    private ArrayList<Double> koot;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        BottleDispenser bd = BottleDispenser.getInstance();
        juomat = new ArrayList<>();
        juomat.add("Cola");
        juomat.add("Fanta");
        juomat.add("EnergyDrink");
        juomat.add("Vesi");

        koot = new ArrayList<>();
        koot.add(0.3);
        koot.add(0.25);
        koot.add(0.5);

        populateBox();

    }

    public void populateBox() {
        for (String drink : juomat) {
            drinkChoiceBox.getItems().add(drink);
        }
        for (double koko : koot) {
            sizeChoiceBox.getItems().add(koko);
        }
    }

    @FXML
    private void sliderChange(MouseEvent event) {
        moneyQuantity.setText("Valittu rahamäärä: " + Math.round(slider.getValue()));

    }

    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Rahaa lisätty koneeseen");
        double money = slider.getValue();
        if (money == 0) {
            label.setText("Valitse rahamäärä ensin!");
        } else {
            BottleDispenser bd = BottleDispenser.getInstance();
            bd.addMoney(Math.round(money));
            label.setText("Rahaa laitteessa: " + bd.getMoney());
            slider.setValue(0);
            moneyQuantity.setText("");
        }
    }

    @FXML
    private void buyBottle(ActionEvent event) {

        BottleDispenser bd = BottleDispenser.getInstance();
        bd.buyBottle(textfield, drinkChoiceBox, sizeChoiceBox,label);
    }

    @FXML
    private void returnMoney(ActionEvent event) {
        BottleDispenser bd = BottleDispenser.getInstance();
        bd.returnMoney(textfield);
        label.setText("Rahaa laitteessa: " + bd.getMoney());

    }

    @FXML
    private void chooseDrink(ActionEvent event) {
        textfield.setText("Valintasi: " + drinkChoiceBox.getValue());
    }

    @FXML
    private void chooseSize(ActionEvent event) {
        textfield.setText(textfield.getText() + " koossa: " + sizeChoiceBox.getValue());
    }

    @FXML
    private void printReceipt(ActionEvent event) {
        String filename = "kuitti.txt";
        try{
            BufferedWriter out = new BufferedWriter(new FileWriter(filename));
            out.flush();
            String line = "*** KUITTI ***\n ostettu juoma: ";
            out.write(line+drinkChoiceBox.getValue()+" koossa "+sizeChoiceBox.getValue());
            out.close();
            textfield.setText("Kuitti tulostettu!");
        }catch (FileNotFoundException ex){
            System.err.println("File not found");
        }catch (IOException e){
            System.err.println("IOException.");
        }
        
    }

}
