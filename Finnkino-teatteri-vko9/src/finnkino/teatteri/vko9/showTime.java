/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finnkino.teatteri.vko9;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author varpu
 */
public class showTime {

    private Document doc;
    private ArrayList<Movie> movieList;
    private ArrayList<Movie> selectedMovieList;

    public ArrayList<Movie> getMovieList() {
        return movieList;
    }

    public ArrayList<Movie> getselectedMovieList() {
        return selectedMovieList;
    }

    public showTime(String content) throws IOException, ParseException {
        movieList = new ArrayList<>();
        selectedMovieList = new ArrayList<>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();

            getShowTimeData();
            //getSelectedShowTime(startHour,endHour);
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(AllCinemas.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void getShowTimeData() throws ParseException {
        NodeList nodes;
        nodes = doc.getElementsByTagName("Show");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            Movie new_movie = new Movie(getValue("Title", e), getSimpleDate(getValue("dttmShowStart", e)), getSimpleDate(getValue("dttmShowEnd", e)),"");
            movieList.add(new_movie);

        }
    }

    public void getSelectedShowTime(String startHour, String endHour) throws ParseException {
        Date wantedHour = getSimpleHour(startHour);
        Date wantedEndHour = getSimpleHour(endHour);
        for (int i = 0; i < movieList.size(); i++) {
            Date startHourKnown = getSimpleHour(movieList.get(i).getShowTime());
            Date endHourKnown = getSimpleHour(movieList.get(i).getEndTime());
            if (startHourKnown.after(wantedHour) || startHourKnown.equals(wantedHour)) {
                if (endHourKnown.equals(wantedEndHour) || endHourKnown.before(wantedEndHour)) {
                    selectedMovieList.add(movieList.get(i));
                }
            }
        }

    }

    private Date getSimpleHour(String h) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("hh:mm");
        Date date = (Date) formatter.parse(h);
        return date;
    }

    private String getSimpleDate(String d) throws ParseException {
        d = d.replace("T", " ");
        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(d);
        return new SimpleDateFormat("HH:mm").format(date);

    }

    private String getValue(String tag, Element e) {
        return e.getElementsByTagName(tag).item(0).getTextContent();
    }
}
