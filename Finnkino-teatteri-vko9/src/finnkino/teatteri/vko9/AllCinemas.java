/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finnkino.teatteri.vko9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author varpu
 */
public class AllCinemas {

    private Document doc;
    private ArrayList<Cinema> list;

    public ArrayList<Cinema> getArrayList() {
        return list;
    }

    public AllCinemas() throws MalformedURLException, IOException {
        list = new ArrayList<>();
        URL url = new URL("https://www.finnkino.fi/xml/TheatreAreas/");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String content = "";
            String line;
            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }
            br.close();
            try {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                doc = dBuilder.parse(new InputSource(new StringReader(content)));
                doc.getDocumentElement().normalize();

                getTheaterData();
            } catch (ParserConfigurationException | SAXException ex) {
                Logger.getLogger(AllCinemas.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    private void getTheaterData() {
        NodeList nodes = doc.getElementsByTagName("TheatreArea");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            Cinema new_cinema = new Cinema(getValue("Name", e), getValue("ID", e));
            list.add(new_cinema);

        }
    }
    
  

    private String getValue(String tag, Element e) {
        return e.getElementsByTagName(tag).item(0).getTextContent();
    }
}
