/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finnkino.teatteri.vko9;



/**
 *
 * @author varpu
 */
public class Movie {

    String title;
    String showTime;
    String showEnd;
    String theatre;

    public String getTitle() {
        return title;
    }

    public String getShowTime() {
        return showTime;
    }

    public String getEndTime() {
        return showEnd;
    }

    public String getTheatre() {
        return theatre;

    }

    public Movie(String t, String st, String se,String th) {
        title = t;
        showTime = st;
        showEnd = se;
        theatre = th;
        

    }

    @Override
    public String toString() {
        return showTime + " " + " " +theatre +" " + title;
    }

}
