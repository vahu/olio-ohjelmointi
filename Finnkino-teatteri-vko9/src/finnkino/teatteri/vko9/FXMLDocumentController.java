/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finnkino.teatteri.vko9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

/**
 *
 * @author varpu
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private ComboBox<Cinema> cityComboBox;
    @FXML
    private TextField showDay;
    @FXML
    private TextField beginHour;
    @FXML
    private TextField endHour;
    @FXML
    private TextField nameSearchField;
    @FXML
    private Button movieListButton;
    @FXML
    private Button nameSearchButton;

    private ArrayList<String> teatterit;
    @FXML
    private ListView<String> listViewMovies;
    @FXML
    private Label infoLabel;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            teatterit = new ArrayList<>();
            AllCinemas finnkino = new AllCinemas();
            cityComboBox.getItems().addAll(finnkino.getArrayList());
            cityComboBox.getSelectionModel().selectFirst();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void listAllMovies(ActionEvent event) throws IOException, ParseException {
        listViewMovies.getItems().clear();
        String date;
        Cinema theatre = cityComboBox.getValue();
        AllCinemas finnkino = new AllCinemas();
        String startHour = beginHour.getText();
        String endingHour = endHour.getText();

        if (showDay.getText().isEmpty()) {
            Date today = new Date();
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            date = dateFormat.format(today);
        } else {
            date = showDay.getText();
        }

        URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + theatre.getID() + "&dt=" + date);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String content = "";
            String line;
            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }
            showTime naytos = new showTime(content);
         
            if (beginHour.getText().isEmpty() && endHour.getText().isEmpty()) {
                Date today = new Date();
                DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                ArrayList lista = naytos.getMovieList();
                listViewMovies.getItems().addAll(lista);
                if (date.equals(dateFormat.format(today))) {
                    infoLabel.setText("Näytökset tänään: ");
                }
                else{
                    infoLabel.setText("Kaikki näytökset päivälle "+ date);
                }

            } else {
                naytos.getSelectedShowTime(startHour, endingHour);
                ArrayList lista2 = naytos.getselectedMovieList();
                listViewMovies.getItems().addAll(lista2);
                infoLabel.setText("Näytökset päivälle " + date + " halutulla aikavälillä: ");

            }

        }

    }

    @FXML
    private void movieNameSearch(ActionEvent event) throws IOException, ParseException {
        listViewMovies.getItems().clear();
        String movieTitle = nameSearchField.getText();
        AllCinemas finnkino = new AllCinemas();
        for (int i = 1; i < finnkino.getArrayList().size(); i++) {
            String identifier = finnkino.getArrayList().get(i).getID();
            String theatreName = finnkino.getArrayList().get(i).getName();
            URL url2 = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + identifier);
            try (BufferedReader br = new BufferedReader(new InputStreamReader(url2.openStream()))) {
                String content = "";
                String line;
                while ((line = br.readLine()) != null) {
                    content += line + "\n";

                }
                showTimeandPlace elokuva = new showTimeandPlace(content, movieTitle, theatreName);
                ArrayList lista = elokuva.getPlacesList();
                listViewMovies.getItems().addAll(lista);
                infoLabel.setText("Näytöspaikat ja ajat elokuvalle: " + ((Movie) lista.get(0)).getTitle());

            }
        }
    }

    @FXML
    private void chooseTheater(MouseEvent event) throws IOException {
        AllCinemas finnkino = new AllCinemas();
    }

}
