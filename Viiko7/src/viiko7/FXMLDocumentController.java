/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viiko7;

import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author varpu
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private TextField inputField;
    @FXML
    private Button button;
    
    /*@FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Text added");
        //label.setText(label.getText()+ inputField.getText()
        //);
        label.setText(label.getText()+"\n");
        inputField.clear();
    }*/
    
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void realTimeTextAdd(javafx.scene.input.KeyEvent event) {
        System.out.println("Adding text");
        label.setText(inputField.getText());
    }
    
}
