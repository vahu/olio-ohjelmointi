/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;


/**
 *
 * @author varpu
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Button button;
    
    @FXML
    private TextField textField;
    
    @FXML
    private TextArea textArea;
    
    
    public void chargeIO(String file){
       
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    
    @FXML
    private void saveFileAction(ActionEvent event) {
        String filename= textField.getText();
        try{
            BufferedWriter out = new BufferedWriter(new FileWriter(filename));
            out.flush();
            String line;
            line = textArea.getText();
            //System.out.print(line);
            out.write(line);
            out.close();
        }catch (FileNotFoundException ex) {
            System.err.println("File not found.");
        } catch (IOException e) {
            System.err.println("IOException.");
        }
        
        
    }

    @FXML
    private void chargeFileAction(ActionEvent event) {
        String filename=textField.getText();
         try{
            BufferedReader in = new BufferedReader(new FileReader(filename));
            String line;
            while ((line=in.readLine()) != null){
                textArea.setText(textArea.getText()+"\n" + line);
            }
            in.close();
        }catch(FileNotFoundException ex){
            System.err.println("File not found");
        }catch (IOException e){
            System.err.println("IOException.");
        }
    }
    
}
