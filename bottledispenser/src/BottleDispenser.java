import java.util.ArrayList;


public class BottleDispenser {
	private int bottles;
	private int money;
	private ArrayList<Bottle> bottle_array;
	
	
	public BottleDispenser(){
		
		bottles = 6;
		money = 0; 
		bottle_array = new ArrayList<Bottle>();
		for (int i = 0; i<bottles;i++){
			Bottle new_bottle = new Bottle();
			bottle_array.add(new_bottle);
		}
		
		
		}

	
	public int removeBottle(){
		int n= bottle_array.size()-1;
		bottle_array.remove(n);
		return bottle_array.size();
		}
	
	public void addMoney(){
	 money += 1;
	 System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
   
	public void buyBottle() {
		int n = bottle_array.size()-1;
		Bottle b = bottle_array.get(n);
		if(n <= 0) {
			System.out.println("Pullot ovat loppuneet!");
		}
		else if(money <= b.getPrice()) {
			System.out.println("Syötä rahaa ensin!");
		}
		else {
			//bottles -= 1;
			money -= 1;
			removeBottle();
			System.out.println("KACHUNK! " + b.getName() + " tipahti masiinasta!");
		}
    }

    public void returnMoney(){
    	money = 0;
    	System.out.println("Klink klink. Sinne menivät rahat!"); 
    }
    
    public void tulostus(){
    	bottles = bottle_array.size();
    	for (int i = 0; i < bottles;i++){
    		Bottle b = bottle_array.get(i);
    		System.out.println(i+1 + "." + "Nimi:"+b.getName() + "\n" + "Koko: "+ b.getSize() + " Hinta: " + b.getPrice());
    	}
    	
    	
    }
}