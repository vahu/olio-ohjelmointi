
import java.util.Scanner;


public class mainclass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BottleDispenser pullokone = new BottleDispenser();
		
		while(true){
			System.out.println("*** LIMSA-AUTOMAATTI ***\n1) Lisää rahaa koneeseen\n2) Osta pullo\n3) Ota rahat ulos\n4) Listaa koneessa olevat pullot\n0) Lopeta");
			Scanner choice = new Scanner(System.in);
			System.out.print("Valintasi:");
			int valinta = choice.nextInt();
		
		if (valinta == 1){
			pullokone.addMoney();
		}
		else if (valinta == 2){
			pullokone.buyBottle();
		}
		else if(valinta == 3){
		pullokone.returnMoney();	
		}
		else if(valinta == 4){
			pullokone.tulostus();
			
		}
		else if (valinta == 0){
			break;
		}
		else{
			System.out.print("Väärä valinta!");
		}
		
		}
		

	}
	

}